import cv2
import cv2.aruco as aruco

dictionary = aruco.Dictionary_get(aruco.DICT_5X5_250)
for id in xrange(1, 6):
    img = aruco.drawMarker(dictionary, id, 700)
    cv2.imwrite("{}.png".format(id), img)
