import cv2
import cv2.aruco as aruco
import numpy as np
import yaml

class ArucoMarkerCalibration:
    '''
    The ids are the marker ids.
    '''
    def __init__(self, dictionary=aruco.DICT_6X6_250):
        self.aruco_dict = aruco.Dictionary_get(dictionary)
        self.parameters =  aruco.DetectorParameters_create()



    def detectCornerMarkers(self, id1, id2, id3, id4):
        find = False
        cap = cv2.VideoCapture(0)
        markerIds = [int(id1), int(id2), int(id3), int(id4)]
        while not find:
            ret, frame = cap.read()
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, self.aruco_dict, parameters=self.parameters)
            if ids is not None:
                A = np.sort(np.array(markerIds))
                B = np.reshape(ids, -1, 4)
                B = np.sort(B)
                find = np.array_equal(A, B)
        cap.release()
        print "Corners found" ###We should have a common log components
        return corners

    def detectGripperMarkers(self, id5):
        cap = cv2.VideoCapture(0)
        find = False
        while not find:
            ret, frame = cap.read()
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, self.aruco_dict, parameters=self.parameters)
            if ids is not None and ids.size!=0 :
                print ids[0]
                find = ids[0]==int(id5)
        cap.release()
        print "found"
        return corners

    def cameraCalibration(self):
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
        objp = np.zeros((6*7,3), np.float32)
        objp[:,:2] = np.mgrid[0:7,0:6].T.reshape(-1,2)
        # Arrays to store object points and image points from all the images.
        objpoints = [] # 3d point in real world space
        imgpoints = [] # 2d points in image plane.

        cap = cv2.VideoCapture(0)
        found = 0
        while(found < 10):  # Here, 10 can be changed to whatever number you like to choose
            ret, img = cap.read() # Capture frame-by-frame
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            # Find the chess board corners
            ret, corners = cv2.findChessboardCorners(gray, (7,6), None)
            # If found, add object points, image points (after refining them)
            if ret == True:
                objpoints.append(objp)  # Certainly, every loop objp is the same, in 3D.
                corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
                imgpoints.append(corners2)
                found += 1
        cap.release()
        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)
        data = {'camera_matrix': np.asarray(mtx).tolist(), 'dist_coeff': np.asarray(dist).tolist()}
        with open("calibration.yaml", "w") as f:
            yaml.dump(data, f)

    def poseEstimation(self, markerLength, corners, file="calibration.yaml"):
        with open(file) as f:
            calibrationParam = yaml.load(f)
        cameraMatrix = calibrationParam.get('camera_matrix')
        distCoeff = calibrationParam.get('dist_coeff')
        rvec, tvec = aruco.estimatePoseSingleMarkers(corners, markerLength, cameraMatrix, distCoeff)
