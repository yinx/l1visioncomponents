import cv2
import cv2.aruco as aruco
import base64
import Tkinter as tk
from pprint import pprint
import copy
import numpy as np

rvecs = np.load('rvecs.npy')
tvecs = np.load('tvecs.npy')
ret = np.load('ret.npy')
mtx = np.load('mtx.npy')
dist = np.load('dist.npy')

dictionary = aruco.Dictionary_get(aruco.DICT_5X5_250)
arucoParams = aruco.DetectorParameters_create()
#arucoParams.adaptiveThreshConstant = 6.14
#arucoParams.adaptiveThreshWinSizeMax = 11
#arucoParams.errorCorrectionRate = 2.537
#arucoParams.perspectiveRemovePixelPerCell = 2
#arucoParams.markerBorderBits = 2
#arucoParams.maxErroneousBitsInBorderRate = 0.183
#arucoParams.doCornerRefinement = 1

img = cv2.imread('1515368296765_rgb.png')
# kinect images need to be flipped
img = cv2.flip(img, 1)
WINDOW_NAME = 'God View - L1 Tuning'

attrs = [
    'adaptiveThreshConstant',
    'adaptiveThreshWinSizeMax',
    'adaptiveThreshWinSizeMin',
    'adaptiveThreshWinSizeStep',
    'cornerRefinementMaxIterations',
    'cornerRefinementMinAccuracy',
    'cornerRefinementWinSize',
    'errorCorrectionRate',
    'markerBorderBits',
    'maxErroneousBitsInBorderRate',
    'maxMarkerPerimeterRate',
    'minCornerDistanceRate',
    'minDistanceToBorder',
    'minMarkerDistanceRate',
    'minMarkerPerimeterRate',
    'minOtsuStdDev',
    'perspectiveRemoveIgnoredMarginPerCell',
    'perspectiveRemovePixelPerCell',
    'polygonalApproxAccuracyRate'
]

def detectAndDisplayMarkers():
    global dictionary, arucoParams, img
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, dictionary,
        parameters=arucoParams)
    print corners, ids
    color = copy.deepcopy(img)
    display = aruco.drawDetectedMarkers(color, corners, ids, borderColor=(0, 255, 0))
    display = aruco.drawDetectedMarkers(display, rejectedImgPoints, borderColor=(255, 0, 0))
    for i in range(0, len(corners)):
        rvec, tvec, discard = aruco.estimatePoseSingleMarkers(corners[i], 1, mtx, dist)
        display = aruco.drawAxis(display, mtx, dist, rvec, tvec, 1)
    cv2.imshow(WINDOW_NAME, display)

def updateValue(event):
    global arucoParams
    value = event.widget.cget('label')
    setattr(arucoParams, value, event.widget.get())
    detectAndDisplayMarkers()

if __name__ == '__main__':
    root = tk.Tk()
    for attr in attrs:
        val = getattr(arucoParams, attr)
        slider = None
        if isinstance(val, int):
            slider = tk.Scale(root, label=attr, from_=0, to=val*3 + 1, orient='horizontal', length = 300)
        else:
            slider = tk.Scale(root, label=attr, resolution=0.001, from_=0, to=val*10 if val > 0 else 1, orient='horizontal', length = 300)
        slider.set(getattr(arucoParams, attr))
        slider.bind("<ButtonRelease-1>", updateValue)
        slider.pack()

    cv2.namedWindow(WINDOW_NAME, cv2.WINDOW_AUTOSIZE)
    cv2.startWindowThread()
    detectAndDisplayMarkers()
    root.mainloop()
    cv2.waitKey(0)
    cv2.destroyAllWindows()
