from roboticarmcalibration import ArucoMarkerCalibration
import sys

#argv = sys.argv
# demo 1 corner marker detection ArucoMarkerCalibration().detectCornerMarkers(argv[1], argv[2], argv[3], argv[4])
# demo 2 arm marker detection ArucoMarkerCalibration().detectGripperMarkers(argv[1])
# demo 3 camera calibration for pose estimation ArucoMarkerCalibration().cameraCalibration()
# demo 4 pose estimation for single marker ArucoMarkerCalibration().poseEstimation(20,corners)
